import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSpelerComponent } from './add-speler.component';

describe('AddSpelerComponent', () => {
  let component: AddSpelerComponent;
  let fixture: ComponentFixture<AddSpelerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSpelerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSpelerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
