import { Component, OnInit } from '@angular/core';
import { HttpClientService, Speler } from '../service/http-client.service';

@Component({
  selector: 'app-add-speler',
  templateUrl: './add-speler.component.html',
  styleUrls: ['./add-speler.component.css']
})
export class AddSpelerComponent implements OnInit {

  spelerToAdd: Speler = new Speler(null, "", "", "", "", "", null, null);

  constructor(
    private httpClientService: HttpClientService
  ) { }

  ngOnInit() {
  }

  createSpeler(): void {
    if (this.spelerToAdd.linienummer == 0) {
      this.spelerToAdd.positie = "Keeper";
    }
    if (this.spelerToAdd.linienummer == 1) {
      this.spelerToAdd.positie = "Verdediger";
    }
    if (this.spelerToAdd.linienummer == 2) {
      this.spelerToAdd.positie = "Middenvelder";
    }
    if (this.spelerToAdd.linienummer == 3) {
      this.spelerToAdd.positie = "Aanvaller";
    }
    this.httpClientService.createSpeler(this.spelerToAdd)
      .subscribe(data => {
        alert("Speler created successfully.");
      });

  };

}
