import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpelerComponent } from './speler/speler.component';
import { AddSpelerComponent } from './add-speler/add-speler.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGaurdService } from './service/auth-gaurd.service';


const routes: Routes = [
  { path: '', component: SpelerComponent, canActivate: [AuthGaurdService] },
  { path: 'addspeler', component: AddSpelerComponent, canActivate: [AuthGaurdService] },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGaurdService] }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
