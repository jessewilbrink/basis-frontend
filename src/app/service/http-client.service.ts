import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export class Speler {
  constructor(
    public id: number,
    public voornaam: string,
    public tussenvoegsel: string,
    public achternaam: string,
    public club: string,
    public positie: string,
    public linienummer: number,
    public waarde: number,
  ) { }
}

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  public getSpelers() {

    let username = 'javainuse'
    let password = 'password'

    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.httpClient.get<Speler[]>('http://localhost:8080/speler/getalle');
  }

  public deleteSpeler(speler) {

    let username = 'javainuse'
    let password = 'password'

    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.httpClient.delete<Speler>("http://localhost:8080/speler/verwijder" + "/" + speler.id);
  }

  public createSpeler(speler) {

    let username = 'javainuse'
    let password = 'password'

    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.httpClient.post<Speler>("http://localhost:8080/speler", speler);
  }
}
