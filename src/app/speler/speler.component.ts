import { Component, OnInit } from '@angular/core';
import { HttpClientService, Speler } from '../service/http-client.service';

@Component({
  selector: 'app-speler',
  templateUrl: './speler.component.html',
  styleUrls: ['./speler.component.css']
})
export class SpelerComponent implements OnInit {
  spelers: Speler[];

  constructor(private httpClientService: HttpClientService) { }

  ngOnInit() {
    this.httpClientService.getSpelers().subscribe(
      Response => this.handleSuccessfulResponse(Response),
    );
  }

  handleSuccessfulResponse(Response) {
    this.spelers = Response;
  }

  deleteSpeler(speler: Speler): void {
    this.httpClientService.deleteSpeler(speler)
      .subscribe(data => {
        this.spelers = this.spelers.filter(u => u !== speler);
      })
  };
}
